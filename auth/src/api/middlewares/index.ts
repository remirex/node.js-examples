import checkJoi from './validateRequest';
import isAuth from './isAuth';
import isAllowed from './isAllowed';

export default {
  checkJoi,
  isAuth,
  isAllowed,
};
