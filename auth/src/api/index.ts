import { Router } from 'express';
import account from './routes/account';

export default () => {
  const app = Router();

  // routes
  account(app);

  return app;
};
