import { Router } from 'express';
import uploadImage from './routes/images';
import uploadFile from './routes/files';

export default () => {
  const app = Router();

  // add route
  uploadImage(app);
  uploadFile(app);

  return app;
};
