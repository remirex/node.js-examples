import { Service, Inject } from 'typedi';

import { EmailTemplates } from '../../interfaces/types';
import { verifyUserEmail } from './templates/verifyUserEmail';
import { alreadyRegisteredEmail } from './templates/alreadyRegisteredEmail';
import { passwordResetEmail } from './templates/passwordResetEmail';
import config from '../../config';

@Service()
export default class EmailService {
  constructor(@Inject('emailClient') private emailClient, @Inject('logger') private logger) {}

  private async sendEmail(to: string, subject: string, content: string) {
    const emailData = {
      from: config.emails.from,
      to: to,
      subject: subject,
      html: content,
    };

    await this.emailClient.sendMail(emailData);

    return { delivered: 1, status: 'ok' };
  }

  public sendTemplateEmail(to: string, subject: string, template: string, data) {
    this.logger.info(`send template email to ${to}`);
    let content = '';
    switch (template) {
      case EmailTemplates.VERIFY_EMAIL:
        content = verifyUserEmail(data);
        break;
      case EmailTemplates.ALREADY_REGISTERED:
        content = alreadyRegisteredEmail(data);
        break;
      case EmailTemplates.RESET_PASSWORD:
        content = passwordResetEmail(data);
        break;
      default:
    }
    return this.sendEmail(to, subject, content);
  }
}
