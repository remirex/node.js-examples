import mongoose from 'mongoose';

import TaskService from '../../src/services/task';
import taskModel from '../../src/models/task';
import LoggerInstance from '../../src/loaders/logger';

const taskService = new TaskService(taskModel, LoggerInstance);

/**
 * Seed the database
 */
beforeEach(async () => {
  await createTasks();
});

describe('Test service unit tests', () => {
  test('should return not found if nothing is found', async () => {
    await expect(taskService.read(`${mongoose.Types.ObjectId()}`)).rejects.toMatch('Task not found');
  });
  test('should retrieve correct task if id matches', async () => {
    const findTask = await taskService.read(taskFirstId);

    expect(findTask.task.id).toBe(taskFirstId);
    expect(findTask.task.name).toBe(taskFirst.name);
  });
});

/**
 * Seed the database with tasks
 */
let taskFirstId;
const createTasks = async () => {
  const createdTask = await taskService.create(taskFirst);
  taskFirstId = createdTask.task.id;
  console.log(taskFirstId);
  await taskService.create(taskSecond);
};

const taskFirst = {
  name: 'first task',
  description: 'first task',
};

const taskSecond = {
  name: 'second task',
  description: 'second task',
};
