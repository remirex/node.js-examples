export enum EmailTemplates {
  VERIFY_EMAIL = 'VERIFY_EMAIL',
  ALREADY_REGISTERED = 'ALREADY_REGISTERED',
  RESET_PASSWORD = 'RESET_PASSWORD',
}

export enum UserStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
  BANNED = 'BANNED',
}

export enum UserRole {
  ADMIN = 'ADMIN',
  MODERATOR = 'MODERATOR',
  GUEST = 'GUEST',
}

export interface IUser {
  id: string;
  email: string;
  password: string;
  username: string;
  firstName: string;
  lastName: string;
  acceptTerms: boolean;
  verificationToken: string;
}

export interface IRefreshToken {
  id: string;
  token: string;
  createdByIp: string;
  revokedByIp: string;
}

export interface IUserInputDTO {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
  acceptTerms: boolean;
}
