import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import { Logger } from 'winston';

import middleware from '../middlewares';
import ImagesService from '../../services/images';

const route = Router();

export default (app: Router) => {
  app.use('/images', route);

  route.post('/upload/single', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Upload Single File endpoint');
    try {
      await middleware.uploadSingleFileMiddleware(req, res);
      if (req.file == undefined) return res.status(400).json({ message: 'You must select a file.' });
      return res.status(201).json({ message: 'File has been uploaded.' });
    } catch (err) {
      logger.error('Error when trying upload image: ', err);
      return next(err);
    }
  });

  route.post('/upload/multiple', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Upload Multiple File endpoint');
    try {
      await middleware.uploadMultipleFileMiddleware(req, res);
      if (req.files.length <= 0) return res.status(400).json({ message: 'You must select at least 1 file.' });
      return res.status(201).json({ message: 'Files have been uploaded.' });
    } catch (err) {
      logger.error('Error when trying upload multiple image: ', err);
      if (err.code === 'LIMIT_UNEXPECTED_FILE') return res.status(400).json({ message: 'To many files to upload.' });
      return next(err);
    }
  });

  route.get('/all', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get the list of image object (in an array) endpoint');
    try {
      const fileServiceInstance = Container.get(ImagesService);
      const files = await fileServiceInstance.getAllFiles();
      return res.status(200).json(files);
    } catch (err) {
      logger.error('Error when trying read images from collection: ', err);
      return next(err);
    }
  });

  route.get('/single/:filename?', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get a single image object endpoint');
    try {
      const filename = req.params.filename;
      if (!filename) return res.status(400).json({ message: 'filename is required.' });
      const fileServiceInstance = Container.get(ImagesService);
      const file = await fileServiceInstance.getSingleFile(filename);
      return res.status(200).json(file);
    } catch (err) {
      logger.error('Error when trying read single image: ', err);
      return next(err);
    }
  });

  route.get('/image/:filename?', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get actual image object endpoint');
    try {
      const filename = req.params.filename;
      if (!filename) return res.status(400).json({ message: 'filename is required.' });
      const fileServiceInstance = Container.get(ImagesService);
      return await fileServiceInstance.getActualFile(filename, res);
    } catch (err) {
      logger.error('Error when trying read actual image: ', err);
      return next(err);
    }
  });

  route.delete('/delete/:id?', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Delete File endpoint');
    try {
      const fileId = req.params.id;
      if (!fileId) return res.status(400).json({ message: 'id is required.' });
      const fileServiceInstance = Container.get(ImagesService);
      const response = await fileServiceInstance.deleteAnFile(fileId);
      return res.status(200).json(response);
    } catch (err) {
      logger.error('Error when trying delete file: ', err);
      return next(err);
    }
  });
};
