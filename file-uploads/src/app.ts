import 'reflect-metadata';
import express from 'express';

import config from './config';
import Logger from './loaders/logger';

async function startServer() {
  const app = express();

  await require('./loaders').default({ expressApp: app });

  app
    .listen(config.port, () => {
      Logger.info(`server is running on port ${config.port} and in ${config.name} mode`);
    })
    .on('error', err => {
      Logger.error(err);
      process.exit(1);
    });
}

startServer().then();
