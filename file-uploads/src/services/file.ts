import { Inject, Service } from 'typedi';
import mongoose from 'mongoose';
import jszip from 'jszip';

/**
 * create a connection via mongoose, initialize a variable for stream(i.e gfs)
 * and once the connection is open, set the gfs variable to Grid(gridfs-stream)
 * and then pass the collection where our images will be stored to gfs
 */
const conn = mongoose.connection;
let gfs;
const gridStream = require('gridfs-stream');
conn.once('open', () => {
  // initialize our stream
  gfs = gridStream(conn.db, mongoose.mongo);
  gfs.collection('documents');
});

// fix findOne bug !!!
eval(`gridStream.prototype.findOne = ${gridStream.prototype.findOne.toString().replace('nextObject', 'next')}`);

const ObjectID = mongoose.Types.ObjectId;

@Service()
export default class FileService {
  constructor(@Inject('logger') private logger) {}

  public async downloadFile(fileId: string, response) {
    try {
      const zip = new jszip();
      // find name by id
      const validId = FileService.isValidId(fileId);
      if (!validId) throw 'Id is not valid';

      let fileName;
      await gfs.findOne({ _id: ObjectID(fileId) }, (err, search) => {
        if (err) {
          console.log(err);
        }
        console.log(search.filename);
        fileName = search.filename;
      });

      const readStream = gfs.createReadStream({ _id: fileId });

      const fileChunks = [] as any;
      //store all chunks into an array
      readStream.on('data', chunk => {
        fileChunks.push(chunk);
      });

      //when all chunks of a files have been read...
      readStream.on('end', async () => {
        //assemble all chunks into a file
        const file = Buffer.concat(fileChunks);
        console.log(file);

        //store file in zip folder
        zip.file(fileName, file, { base64: true });
        //when all downloaded files are in zip folder...
        await zip
          .generateAsync({ type: 'nodebuffer' })
          .then(content => {
            //deliver zip folder to front end
            response.send(content);
          })
          .catch(err => {
            throw err;
          });
      });
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async downloadFiles(files: string[], response) {
    try {
      const lists = files;
      const zip = new jszip();
      let downloaded = 0;

      lists.map(async (item, index) => {
        let fileName;
        await gfs.findOne({ _id: item }, (err, search) => {
          if (err) {
            // console.log(err)
          }

          // console.log(search.filename);
          fileName = search.filename;
        });

        const readStream = gfs.createReadStream({ _id: item });

        const fileChunks = [] as any;
        //store all chunks into an array
        readStream.on('data', chunk => {
          fileChunks.push(chunk);
        });

        //when all chunks of a files have been read...
        readStream.on('end', async () => {
          //assemble all chunks into a file
          const file = Buffer.concat(fileChunks);
          console.log(file);
          //store file in zip folder
          await zip.file(fileName, file, { base64: true });
          console.log(index);
          //record # of files downloaded
          downloaded++;
          //when all downloaded files are in zip folder...
          if (downloaded === lists.length) {
            await zip
              .generateAsync({ type: 'nodebuffer' })
              .then(content => {
                //deliver zip folder to font end
                response.send(content);
              })
              .catch(err => {
                throw err;
              });
          }
        });
      });
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  // helper function
  private static isValidId(id: string) {
    return mongoose.Types.ObjectId.isValid(id);
  }
}
