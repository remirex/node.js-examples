import { NextFunction, Request, Response, Router } from 'express';
import { Container } from 'typedi';
import { Logger } from 'winston';

import middleware from '../middlewares';
import FileService from '../../services/file';

const route = Router();

export default (app: Router) => {
  app.use('/files', route);

  route.post('/upload', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Upload File endpoint');
    try {
      await middleware.uploadFileMiddleware(req, res);
      if (req.file === undefined) return res.status(400).json({ message: 'Please upload a file.' });

      return res.status(200).json({ message: `Uploaded the file successfully: ${req.file.originalname}` });
    } catch (err) {
      logger.error('Error when trying upload file: ', err);
      if (err.code === 'LIMIT_FILE_SIZE')
        return res.status(500).json({ message: 'File size cannot be larger than 2MB.' });
      return next(err);
    }
  });

  route.get('/download/:id?', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Download File endpoint');
    try {
      const fileId = req.params.id;
      if (!fileId) return res.status(400).json({ message: 'id is required.' });
      const fileServiceInstance = Container.get(FileService);
      return await fileServiceInstance.downloadFile(fileId, res);
    } catch (err) {
      logger.error('Error when trying download file: ', err);
      return next(err);
    }
  });

  route.post('/download', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Download Files endpoint');
    try {
      const files = req.body.files;
      if (files.length == 0) return res.status(400).json({ message: 'Please set one or more files.' });
      const fileServiceInstance = Container.get(FileService);
      return await fileServiceInstance.downloadFiles(files, res);
    } catch (err) {
      logger.error('Error when trying download files: ', err);
      return next(err);
    }
  });
};
