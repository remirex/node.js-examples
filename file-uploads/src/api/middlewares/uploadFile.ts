import util from 'util';
import multer from 'multer';
import GridFsStorage from 'multer-gridfs-storage';

import config from '../../config';

const maxSize = 2 * 1024 * 1024; // max 2MB

const storage = new GridFsStorage({
  url: String(config.databaseURL),
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file(req: Express.Request, file: Express.Multer.File): any {
    return {
      bucketName: 'documents',
      filename: `${Date.now()}-remirex-${file.originalname}`,
    };
  },
});

const uploadFile = multer({ storage: storage, limits: { fileSize: maxSize } }).single('file');
export const uploadFileMiddleware = util.promisify(uploadFile);
