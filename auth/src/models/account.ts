import mongoose from 'mongoose';

import { Password } from '../services/password';
import { IUser } from '../interfaces/types';

const schema = new mongoose.Schema(
  {
    email: {
      type: String,
      unique: true,
      required: true,
    },
    username: {
      type: String,
      unique: true,
      required: true,
    },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    acceptTerms: { type: Boolean, required: true },
    role: {
      type: String,
      // default: 'guest',
      enum: ['GUEST', 'ADMIN'],
    },
    status: {
      type: String,
      default: 'INACTIVE',
      enum: ['INACTIVE', 'ACTIVE', 'BANED'],
    },
    verificationToken: {
      token: String,
      expires: Date,
    },
    verified: Date,
    resetToken: {
      token: String,
      expires: Date,
    },
    passwordReset: Date,
  },
  {
    timestamps: true,
    toJSON: {
      virtuals: true,
      versionKey: false,
      transform(doc, ret) {
        // remove these props when object is serialized
        delete ret._id;
        delete ret.__v;
        delete ret.password;
        delete ret.verificationToken;
        delete ret.resetToken;
        // transform
        ret.id = ret._id;
      },
    },
  },
);

schema.pre('save', async function (done) {
  if (this.isModified('password')) {
    const hashed = await Password.toHash(this.get('password'));
    this.set('password', hashed);
  }
  done();
});

schema.virtual('isVerified').get(function (this: { verified: Date; passwordReset: Date }): boolean {
  return !!(this.verified || this.passwordReset);
});

export default mongoose.model<IUser & mongoose.Document>('User', schema);
