import { Container } from 'typedi';
import nodemailer from 'nodemailer';

import LoggerInstance from './logger';
import agendaFactory from './agenda';
import config from '../config';

export default ({ mongoConnection, models }: { mongoConnection; models: { name: string; model }[] }) => {
  try {
    models.map(m => {
      Container.set(m.name, m.model);
    });

    const agendaInstance = agendaFactory({ mongoConnection });
    Container.set('agendaInstance', agendaInstance);

    // Logger instance
    Container.set('logger', LoggerInstance);

    // Nodemailer instance
    Container.set(
      'emailClient',
      nodemailer.createTransport({
        host: config.emails.host,
        port: config.emails.port,
        secure: false, // true for 465, false for other ports
        auth: {
          user: config.emails.user,
          pass: config.emails.pass,
        },
      }),
    );

    LoggerInstance.info('Agenda injected into container');

    return { agenda: agendaInstance };
  } catch (err) {
    LoggerInstance.error('Error on dependency injector loader: ', err);
    throw err;
  }
};
