import checkJoi from './validateRequest';
import { uploadSingleFileMiddleware } from './uploadImage';
import { uploadMultipleFileMiddleware } from './multipleImagesUpload';
import { uploadFileMiddleware } from './uploadFile';

export default {
  checkJoi,
  uploadSingleFileMiddleware,
  uploadMultipleFileMiddleware,
  uploadFileMiddleware,
};
