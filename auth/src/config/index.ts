import dotenv from 'dotenv';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (envFound.error) {
  // This error should crash whole process

  throw new Error(`"Couldn't find .env file"`);
}

export default {
  /**
   * Your favorite port
   */
  port: parseInt(String(process.env.PORT), 10),
  name: process.env.NODE_ENV,

  /**
   * That long string from mlab
   */
  databaseURL: process.env.MONGODB_URI,

  /**
   * Your secret sauce
   */
  jwtSecret: String(process.env.JWT_SECRET),
  jwtAlgorithm: String(process.env.JWT_ALGO),

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'silly',
  },

  /**
   * API configs
   */
  api: {
    prefix: '/api',
  },

  /**
   * Agenda.js stuff
   */
  agenda: {
    dbCollection: process.env.AGENDA_DB_COLLECTION,
    pooltime: process.env.AGENDA_POOL_TIME,
    concurrency: parseInt(process.env.AGENDA_CONCURRENCY!, 10),
  },

  agendash: {
    user: 'agendash',
    password: '123456',
  },

  /**
   * Nodemailer email credentials
   */
  emails: {
    user: String(process.env.EMAIL_USER),
    pass: String(process.env.EMAIL_PASS),
    from: String(process.env.EMAIL_FROM),
    host: String(process.env.EMAIL_HOST),
    port: Number(process.env.EMAIL_PORT),
  },

  /**
   * Client
   */
  clientUrl: process.env.CLIENT_URI,
};
