import { Inject, Service } from 'typedi';
import cryptoRandomString from 'crypto-random-string';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';

import EmailService from './emailService/emailService';
import { EmailTemplates, IUserInputDTO, UserRole } from '../interfaces/types';
import { UserStatus } from '../interfaces/types';
import { Password } from './password';
import config from '../config';

@Service()
export default class Account {
  constructor(
    @Inject('userModel') private userModel: Models.UserModel,
    @Inject('refreshTokenModel') private refreshTokenModel: Models.RefreshTokenModel,
    @Inject('logger') private logger,
    private mailer: EmailService,
    private password: Password,
  ) {}

  public async register(userInputDTO: IUserInputDTO): Promise<{ message: string }> {
    // validate
    const account = await this.userModel.findOne({ email: userInputDTO.email });

    if (account && account.status === UserStatus.INACTIVE) {
      // update expire date
      account.verificationToken.expires = new Date(Date.now() + 24 * 60 * 60 * 1000);
      account.save();
      await this.mailer.sendTemplateEmail(
        account.email,
        'Sign-up Verification API - Verify Email',
        EmailTemplates.VERIFY_EMAIL,
        {
          firstName: account.firstName,
          lastName: account.lastName,
          token: account.verificationToken.token,
        },
      );
      throw 'Register but not verified, please check your email for verification instructions';
    }

    if (account) {
      // send already registered error in email to prevent account enumeration
      await this.mailer.sendTemplateEmail(
        userInputDTO.email,
        'Sign-up Verification API - Email Already Registered',
        EmailTemplates.ALREADY_REGISTERED,
        account,
      );
      throw 'You are already registered';
    }

    const findUsername = await this.userModel.findOne({ username: userInputDTO.username });

    if (findUsername) throw `Username ${userInputDTO.username} is already exist in database.`;

    // first registered account is an admin
    const isFirstAccount = (await this.userModel.countDocuments({})) === 0;

    this.logger.silly('Creating user db record');
    const verifyToken = cryptoRandomString({ length: 32, type: 'url-safe' });
    const expireToken = new Date(Date.now() + 24 * 60 * 60 * 1000); // create verify token that expires after 24 hours
    // create account object
    const userRecord = await this.userModel.create({
      ...userInputDTO,
      verificationToken: {
        token: verifyToken,
        expires: expireToken,
      },
      role: isFirstAccount ? UserRole.ADMIN : UserRole.GUEST,
    });

    if (!userRecord) throw 'User cannot be created';

    this.logger.silly('Sending verify email');
    await this.mailer.sendTemplateEmail(
      userInputDTO.email,
      'Sign-up Verification API - Verify Email',
      EmailTemplates.VERIFY_EMAIL,
      {
        firstName: userInputDTO.firstName,
        lastName: userInputDTO.lastName,
        token: verifyToken,
      },
    );

    return { message: 'Registration successful, please check your email for verification instructions' };
  }

  public async verifyEmail(token: string): Promise<{ message: string }> {
    const account = await this.userModel.findOne({
      'verificationToken.token': token,
      'verificationToken.expires': { $gt: Date.now() },
    });

    if (!account) throw 'Invalid token';

    account.verified = Date.now();
    account.status = UserStatus.ACTIVE;
    account.verificationToken = undefined;
    account.save();

    return { message: 'Verification successful, you can now login' };
  }

  public async authenticate(email: string, password: string, ipAddress: string) {
    const account = await this.userModel.findOne({ email: email });

    if (!account) throw 'User not registered.';

    if (account.status !== UserStatus.ACTIVE) throw 'User not verified yet';

    if (account.status === UserStatus.BANNED) throw 'User is banned';

    // compare password
    this.logger.silly('Checking password');
    const validPassword = await this.password.compare(account.password, password);
    console.log(validPassword);
    if (!validPassword) throw 'Invalid password';
    this.logger.silly('Password is valid!');

    this.logger.silly('Generating JWT');
    // authentication successful so generate jwt and refresh tokens
    const jwtToken = await Account.generateJwtToken(account);
    const refreshToken = await this.generateRefreshToken(account, ipAddress);

    return {
      auth: true,
      jwtToken,
      refreshToken: refreshToken.token,
    };
  }

  public async refreshToken(token: string, ipAddress: string) {
    const refreshToken = await this.getRefreshToken(token);
    const { account } = refreshToken;

    // replace old refresh token with a new one and save
    const newRefreshToken = await this.generateRefreshToken(account, ipAddress);
    refreshToken.revoked = Date.now();
    refreshToken.revokedByIp = ipAddress;
    refreshToken.replacedByToken = newRefreshToken.token;

    await refreshToken.save();

    // generate new jwt
    const jwtToken = await Account.generateJwtToken(account);

    return {
      auth: true,
      jwtToken,
      refreshToken: newRefreshToken.token,
    };
  }

  public async revokeToken(authHeader: string, token: string, ipAddress: string): Promise<{ message: string }> {
    const refreshToken = await this.getRefreshToken(token);

    // users can revoke their own tokens and admins can revoke any tokens
    const isOwned = await this.ownsToken(refreshToken, authHeader);
    if (!isOwned) throw 'User is not owner for this token';

    // revoke token and save
    refreshToken.revoked = Date.now();
    refreshToken.revokedByIp = ipAddress;
    await refreshToken.save();

    return { message: 'Token revoked' };
  }

  public async checkEmail(email: string) {
    const findEmail = await this.userModel.findOne({ email: email });
    return !!findEmail;
  }

  public async checkUsername(username: string) {
    const findUsername = await this.userModel.findOne({ username: username });
    return !!findUsername;
  }

  public async forgotPassword(email: string): Promise<{ message: string }> {
    const account = await this.userModel.findOne({ email: email });

    // always return ok response to prevent email enumeration
    if (!account) throw 'User not found';

    // create reset token that expires after 24 hours
    account.resetToken = {
      token: Account.randomTokenString(),
      expires: new Date(Date.now() + 24 * 60 * 60 * 1000),
    };

    await account.save();

    // send email
    await this.mailer.sendTemplateEmail(
      email,
      'Sign-up Verification API - Reset Password',
      EmailTemplates.RESET_PASSWORD,
      {
        username: account.username,
        token: account.resetToken.token,
      },
    );

    return { message: 'Please check your email for password reset instructions' };
  }

  public async validateResetToken(token: string) {
    const account = await this.userModel.findOne({
      'resetToken.token': token,
      'resetToken.expires': { $gt: Date.now() },
    });

    if (!account) throw 'Invalid token';
  }

  public async resetPassword(token: string, newPassword: string): Promise<{ message: string }> {
    const account = await this.userModel.findOne({
      'resetToken.token': token,
      'resetToken.expires': { $gt: Date.now() },
    });

    if (!account) throw 'Invalid token';

    // update password and remove reset token
    const hash = await this.password.toHash(newPassword);
    await this.userModel.updateOne(
      { 'resetToken.token': token },
      { $set: { password: hash, passwordReset: Date.now(), resetToken: undefined } },
    );

    return { message: 'Password reset successful, you can now login' };
  }

  // helper functions

  private static async generateJwtToken(account: { id: string; role: string }) {
    // create a jwt token containing the account id that expires in 15 minutes
    return jwt.sign({ sub: account.id, id: account.id, role: account.role }, config.jwtSecret, {
      expiresIn: '15m',
    });
  }

  private async getRefreshToken(token: string) {
    const refreshToken = await this.refreshTokenModel.findOne({ token }).populate('User');
    if (!refreshToken || !refreshToken.isActive) throw 'Invalid token';
    return refreshToken;
  }

  private async generateRefreshToken(account: { id: string }, ipAddress: string) {
    // create a refresh token that expires in 7 days
    return await this.refreshTokenModel.create({
      account: account.id,
      token: Account.randomTokenString(),
      expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
      createdByIp: ipAddress,
    });
  }

  private static randomTokenString() {
    return crypto.randomBytes(40).toString('hex');
  }

  private async ownsToken(refreshToken: { token: string }, authHeader: string) {
    // decode token
    const decoded: any = jwt.decode(authHeader);

    // find tokens & account
    const account = await this.userModel.findById(decoded.id);
    const refreshTokens = await this.refreshTokenModel.find({ account: account._id });

    // check if tokens match
    const found = refreshTokens.some(item => {
      return item.token === refreshToken.token;
    });

    return !!found;
  }
}
