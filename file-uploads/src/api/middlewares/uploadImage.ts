import util from 'util';
import multer from 'multer';
import GridFsStorage from 'multer-gridfs-storage';

import config from '../../config';

const storage = new GridFsStorage({
  url: String(config.databaseURL),
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file(req: Express.Request, file: Express.Multer.File): any {
    const match = ['image/png', 'image/jpeg'];

    if (match.indexOf(file.mimetype) === -1) {
      return `${Date.now()}-remirex-${file.originalname}`;
    }

    return {
      bucketName: 'photos',
      filename: `${Date.now()}-remirex-${file.originalname}`,
    };
  },
});

const uploadFile = multer({ storage: storage }).single('file');
export const uploadSingleFileMiddleware = util.promisify(uploadFile);
