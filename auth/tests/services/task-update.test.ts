import mongoose from 'mongoose';

import TaskService from '../../src/services/task';
import taskModel from '../../src/models/task';
import LoggerInstance from '../../src/loaders/logger';

const taskService = new TaskService(taskModel, LoggerInstance);

/**
 * Seed the database
 */
beforeEach(async () => {
  await createTasks();
});

describe('Test service unit tests', () => {
  test('should return not found if nothing is found', async () => {
    await expect(taskService.read(`${mongoose.Types.ObjectId()}`)).rejects.toMatch('Task not found');
  });
  test('should update task if id matches', async () => {
    const updatedTask = await taskService.update(updatedTaskInfo, taskFirstId);
    console.log(updatedTask);
    expect(updatedTask.task.name).toBe('updated task');
    expect(updatedTask.task.description).toBe('updated task');
  });
});

/**
 * Seed the database with tasks
 */
let taskFirstId;
const createTasks = async () => {
  const createdTask = await taskService.create(taskFirst);
  taskFirstId = createdTask.task.id;
  console.log(taskFirstId);
  await taskService.create(taskSecond);
};

const taskFirst = {
  name: 'first task',
  description: 'first task',
};

const taskSecond = {
  name: 'second task',
  description: 'second task',
};

const updatedTaskInfo = {
  name: 'updated task',
  description: 'updated task',
};
