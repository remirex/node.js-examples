import { IRefreshToken } from '../interfaces/types';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const schema = new Schema({
  account: { type: Schema.Types.ObjectId, ref: 'User' },
  token: String,
  expires: Date,
  created: { type: Date, default: Date.now },
  createdByIp: String,
  revoked: Date,
  revokedByIp: String,
  replacedByToken: String,
});

schema.virtual('isExpired').get(function (this: { expires: any }): boolean {
  return Date.now() >= this.expires;
});

schema.virtual('isActive').get(function (this: { revoked: any; isExpired: boolean }): boolean {
  return !this.revoked && !this.isExpired;
});

export default mongoose.model<IRefreshToken & mongoose.Document>('RefreshToken', schema);
