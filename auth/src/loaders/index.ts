import expressLoader from './express';
import mongooseLoader from './mongoose';
import dependencyInjectorLoader from './dependencyInjector';
import Logger from './logger';

export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader();
  Logger.info('DB loaded and connected');

  const userModel = {
    name: 'userModel',
    // Notice the require syntax and the '.default'
    model: require('../models/account').default,
  };

  const refreshTokenModel = {
    name: 'refreshTokenModel',
    // Notice the require syntax and the '.default'
    model: require('../models/refresh-token').default,
  };

  await dependencyInjectorLoader({
    mongoConnection,
    models: [userModel, refreshTokenModel],
  });
  Logger.info('Dependency Injector loaded');

  await expressLoader({ app: expressApp });
  Logger.info('Express loaded');
};
