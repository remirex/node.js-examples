# Node.js architecture
The API it's just a single/multiple images upload to MongoDB.

## Setup the necessary tools

- Express: basically a Node.js web application framework.
- Mongoose: Object Data Modeling (ODM) library for MongoDB and Node.js. It basically handles relationship between data.
- Multer: Is used for uploading files.
- Gridfs-stream: Allows streaming of files to and from mongodb.
- Gridfs: This is a specification for storing and retriviing files that excess the BSON-document size limit of 16MB
- multer-gridfs-storage: Multer's GridFS storage engine

## Development
We use `node` version `v10.19.0`
```
nvm install 10.19.0
```
```
nvm use 10.19.0
```
The first time, you will need to run
```
npm install
```
Then just start server with
```
npm run start
```


