import { Container } from 'typedi';

import LoggerInstance from './logger';

export default () => {
  try {
    // Logger instance
    Container.set('logger', LoggerInstance);

    LoggerInstance.info('Agenda injected into container');
  } catch (err) {
    LoggerInstance.error('Error on dependency injector loader: ', err);
    throw err;
  }
};
