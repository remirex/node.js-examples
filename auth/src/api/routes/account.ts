import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import { Logger } from 'winston';
import Joi from 'joi';

import AuthService from '../../services/account';
import { IUserInputDTO, UserRole } from '../../interfaces/types';
import middlewares from '../middlewares';

const route = Router();

export default (app: Router) => {
  app.use('/account', route);

  route.post('/register', createSchema, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Register endpoint with body: %o', req.body);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.register(req.body as IUserInputDTO);
      return res.status(201).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });

  route.post('/verify-email', verifySchema, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Verify Email endpoint with body: %o', req.body);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.verifyEmail(req.body.token);
      return res.status(200).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });

  route.post('/authenticate', authSchema, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Authenticate endpoint with body: %o', req.body);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.authenticate(req.body.email, req.body.password, req.ip);
      return res.status(200).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });

  route.post('/refresh-token', refreshTokenSchema, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Refresh Token endpoint with body: %o', req.body);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.refreshToken(req.body.token, req.ip);
      return res.status(200).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });

  route.post(
    '/revoke-token',
    revokeTokenSchema,
    middlewares.isAuth,
    middlewares.isAllowed([UserRole.ADMIN, UserRole.GUEST]),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Revoke Token endpoint with body: %o', req.body);
      try {
        let authHeader;
        if (
          (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token') ||
          (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
        ) {
          authHeader = req.headers.authorization.split(' ')[1];
        }

        const authServiceInstance = Container.get(AuthService);
        const response = await authServiceInstance.revokeToken(authHeader, req.body.token, req.ip);
        return res.status(200).json(response);
      } catch (error) {
        logger.error('🔥 error: %o', error);
        return next(error);
      }
    },
  );

  route.get('/check-email/:email', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Check Email endpoint with req params: %o', req.params);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.checkEmail(req.params.email);
      return res.status(200).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });

  route.get('/check-username/:username', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Check Username endpoint with req params: %o', req.params);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.checkUsername(req.params.username);
      return res.status(200).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });

  route.post('/forgot-password', forgotPasswordSchema, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Forgot Password endpoint with req params: %o', req.body);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.forgotPassword(req.body.email);
      return res.status(200).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });

  route.post(
    '/validate-reset-token',
    validateResetTokenSchema,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Validate Reset Token Password endpoint with req params: %o', req.body);
      try {
        const authServiceInstance = Container.get(AuthService);
        const response = await authServiceInstance.validateResetToken(req.body.token);
        return res.status(200).json(response);
      } catch (error) {
        logger.error('🔥 error: %o', error);
        return next(error);
      }
    },
  );

  route.post('/reset-password', resetPasswordSchema, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Check Reset Password endpoint with req params: %o', req.body);
    try {
      const authServiceInstance = Container.get(AuthService);
      const response = await authServiceInstance.resetPassword(req.body.token, req.body.password);
      return res.status(200).json(response);
    } catch (error) {
      logger.error('🔥 error: %o', error);
      return next(error);
    }
  });
};

function createSchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'io'] } })
      .required(),
    username: Joi.string().alphanum().min(3).max(30).required(),
    password: Joi.string().required().min(8).max(20).pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    repeatPassword: Joi.ref('password'),
    acceptTerms: Joi.boolean().required().invalid(false),
  }).with('password', 'repeatPassword');
  middlewares.checkJoi(req, res, next, schema);
}

function verifySchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    token: Joi.string().required(),
  });
  middlewares.checkJoi(req, res, next, schema);
}

function authSchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'io'] } })
      .required(),
    password: Joi.string().required(),
  });
  middlewares.checkJoi(req, res, next, schema);
}

function refreshTokenSchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    token: Joi.string().required(),
  });
  middlewares.checkJoi(req, res, next, schema);
}

function revokeTokenSchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    token: Joi.string().required(),
  });
  middlewares.checkJoi(req, res, next, schema);
}

function forgotPasswordSchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'io'] } })
      .required(),
  });
  middlewares.checkJoi(req, res, next, schema);
}

function validateResetTokenSchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    token: Joi.string().required(),
  });
  middlewares.checkJoi(req, res, next, schema);
}

function resetPasswordSchema(req: Request, res: Response, next: NextFunction) {
  const schema = Joi.object({
    token: Joi.string().required(),
    password: Joi.string().required().min(8).max(20).pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  });
  middlewares.checkJoi(req, res, next, schema);
}
