import { NextFunction, Request, Response } from 'express';

/**
 * The validate request middleware function validates the body of a request against a Joi schema object.
 * @param req
 * @param res
 * @param next
 * @param schema
 */
export default (req: Request, res: Response, next: NextFunction, schema) => {
  const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true, // remove unknown props
  };
  const { error, value } = schema.validate(req.body, options);
  if (error) {
    const { details } = error;
    const errorsDetail = details.map(i => i);
    console.log(errorsDetail);
    res.status(422).json({
      status: false,
      error: errorsDetail,
    });
  } else {
    req.body = value;
    next();
  }
};
