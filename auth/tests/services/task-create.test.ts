import mongoose from 'mongoose';

import TaskService from '../../src/services/task';
import taskModel from '../../src/models/task';
import LoggerInstance from '../../src/loaders/logger';

const taskService = new TaskService(taskModel, LoggerInstance);

describe('Test service unit tests', () => {
  describe('Create', () => {
    test('can be created correctly', async () => {
      expect(async () => {
        await taskService.create(taskComplete);
      }).not.toThrow();
    });
    test('exists after being created', async () => {
      await taskService.create(taskComplete);

      const createdProduct = await taskModel.findOne();

      expect(createdProduct!.name).toBe(taskComplete.name);
    });
    test('requires name & description', async () => {
      await expect(taskModel.create(taskMissingName)).rejects.toThrow(mongoose.Error.ValidationError);

      await expect(taskModel.create(taskMissingDescription)).rejects.toThrow(mongoose.Error.ValidationError);
    });
  });
});

const taskComplete = {
  name: 'test name',
  description: 'test description',
};

const taskMissingName = {
  name: '',
  description: 'test description',
};

const taskMissingDescription = {
  name: 'test name',
  description: '',
};
