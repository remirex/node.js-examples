import { Inject, Service } from 'typedi';
import Grid from 'gridfs-stream';
import mongoose from 'mongoose';

/**
 * create a connection via mongoose, initialize a variable for stream(i.e gfs)
 * and once the connection is open, set the gfs variable to Grid(gridfs-stream)
 * and then pass the collection where our images will be stored to gfs
 */
const conn = mongoose.connection;
let gfs;
conn.once('open', () => {
  // initialize our stream
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection('photos');
});

@Service()
export default class ImagesService {
  constructor(@Inject('logger') private logger) {}

  /**
   * Get the list of image object (in an array)
   */
  public async getAllFiles() {
    try {
      const files = await gfs.files.find().toArray();
      // check if files exist
      if (!files || files.length == 0) throw 'Files not found.';
      // files exist
      return files;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  /**
   * To get a single file, all we need is the filename and we can call a findOne on gfs i.e
   * @param filename
   */
  public async getSingleFile(filename: string) {
    try {
      const singleFile = await gfs.files.findOne({ filename });
      // check if file exist
      if (!singleFile || singleFile.length == 0) throw 'File not found';
      // file exist
      return singleFile;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  /**
   * To get the image itself
   * @param filename
   * @param response
   */
  public async getActualFile(filename: string, response) {
    try {
      const singleFile = await gfs.files.findOne({ filename });
      // check if file exist
      if (!singleFile || singleFile.length == 0) throw 'File not found';
      // check if not image
      if (singleFile.contentType === 'image/png' || singleFile.contentType === 'image/jpeg') {
        //read output to browser
        const readStream = gfs.createReadStream(singleFile.filename);
        return readStream.pipe(response);
      } else {
        throw 'Not an image';
      }
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  /**
   * Delete an image
   * @param id
   */
  public async deleteAnFile(id: string) {
    try {
      const validId = ImagesService.isValidId(id);
      if (!validId) throw 'Id is not valid';

      await gfs.remove({ _id: id, root: 'photos' });
      return null;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  // helper function
  private static isValidId(id: string) {
    return mongoose.Types.ObjectId.isValid(id);
  }
}
