import { Document, Model } from 'mongoose';
import { IUser, IRefreshToken } from '../../interfaces/types';

declare global {
  namespace Models {
    export type UserModel = Model<IUser & Document>;
    export type RefreshTokenModel = Model<IRefreshToken & Document>;
  }
}
